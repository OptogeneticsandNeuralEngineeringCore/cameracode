#!/usr/bin/env python
# coding: utf-8

# In[2]:


# Import Libraries 
import os
import time
import numpy as np
from gfd.py.video.capture import VideoCaptureThreading
import hashlib
import subprocess
import cv2
import matplotlib.pyplot as plt
import re
import json


# In[3]:


def list_usb_devices():
    """Lists all usb devices currently available"""
    termcmd = "v4l2-ctl --list-devices"
    ps = subprocess.Popen(termcmd,
                      shell=True,
                      stdout=subprocess.PIPE,
                      stderr=subprocess.STDOUT,
                      universal_newlines=True)
    output = ps.communicate()[0]
    print ("Detected USB Devices \n\n" + str(output))    
    return

class MultiCam:
    """Class for running video capture from multiple webcams"""

    def __init__(self, cam_nums=[1,2], width=640, height=480,
                 out_dir=None, vid_prefixes=[], printout = True, 
                 json_filename=[], cam_types = [''], timestamps_file_name=''):
        self.cam_nums = cam_nums
        print("Cam nums: " + str(self.cam_nums))
        self.width = width # of each frame in pixels
        self.height = height # of each frame in pixels
        self.vid_prefixes = vid_prefixes # beginning of filesnames
        self.cam_props = {} # for storing all camera properties
        self.printout = printout # flag for turning on/off print out
        self.cam_types = cam_types # for storing camera type
        self.json_filename = json_filename
        self.timestamps_file_name = timestamps_file_name
        
        # Set up camera objects
        self.cameras = []
        for cam_num in self.cam_nums:
            self.cameras += [VideoCaptureThreading(cam_num, self.width, self.height)]
        
        # Handle video prefixes/filenames
        if not self.vid_prefixes:
            for cam_num in self.cam_nums:
                self.vid_prefixes += ["video_cam" + str(cam_num)]
        assert len(self.vid_prefixes) == len(cam_nums), "\nERROR!\nThe number of specified cameras does not equal the number of specified videos.\nEnding Recording"
        print("Video(s) will be saved with the file prefix(es):")
        for prefix in self.vid_prefixes:
            print(prefix)
        
        # Set the output directory
        if not out_dir:
            out_dir = out_dir=os.getcwd()
        else: 
            assert os.path.isdir(out_dir) == True, "\nERROR!\That directory doesn't exist.\nPlease update"
        self.out_dir = out_dir # directory to store output files
        print("Output directory: " + out_dir)
        
    def fetch_camera_properties(self, cam_types=[''], printout = True):
        """Get current camera property values from cameras"""
        
        camera_numbers =self.cam_nums
        if printout:
            print("Fetching camera properties for camera numbers: " + str(camera_numbers))
        camera_properties = []
        
        # Handle if empty list passed in
        if not cam_types:
            cam_types = ['']
            
        # Define camera properties
        if cam_types[0] == 'ps3eye':
            if printout:
                print('Cameras were set as ps3eye cameras')
            camera_properties = [
                'brightness',
                'contrast',
                'saturation',
                'hue',
                'white_balance_automatic',
                'auto_exposure', #counterintuitively turns off auto exposure when 1
                'exposure',
                'gain_automatic',
                'gain',
                'horizontal_flip',
                'vertical_flip',
                'power_line_frequency',
                'sharpness']
        elif cam_types[0] == 'logitech':
            if printout:
                print('Cameras were set as logitech cameras')
            camera_properties = [
                'brightness',
                'contrast',
                'saturation',
                'hue', 
                'white_balance_temperature_auto',
                'gamma',
                'white_balance_temperature', #(int) : min=2800 max=6500 step=10 default=4600 value=4600 flags=inactive
                'sharpness',
                'backlight_compensation', #(int) : min=0 max=1 step=1 default=0 value=0
                'exposure_auto', #counterintuitively turns off auto exposure
                'exposure_absolute',
                'exposure_auto_priority']
        else:
            if printout:
                print("WARNING! Unsupported camera type selected! Code will attempt to work.")
            camera_properties = [
                'brightness',
                'contrast',
                'saturation']

        # Fetch camera properties
        if printout:
            print('Cameras were found to have the following parameters: ')
        all_cam_props = {}
        for camera_number in camera_numbers:
            all_cam_props[camera_number] = {}
            print("\nCamera Number " + str(camera_number))
            for cam_prop in camera_properties:
                proc = subprocess.Popen('v4l2-ctl -d /dev/video{} --get-ctrl {}'.format(camera_number, cam_prop), 
                                        stdout=subprocess.PIPE, 
                                        shell=True)
                output = str(proc.stdout.read())
                if printout:
                    print('   ', output[2:-3])
                if cam_prop in output:
                    new_output = output.replace("\\", "")
                    try:
                        found = re.search(': (.+?)n', new_output).group(1)
                    except AttributeError:
                        found = '' # apply your error handling
                    all_cam_props[camera_number][cam_prop] = found
                    
        self.cam_props = all_cam_props
        self.cam_types = cam_types
        return all_cam_props
                    
    def save_cam_params(self, 
                        cam_params_txt_file='camera_params', 
                        timestampprop=True,
                        timestring = '',
                        printout = False):
        """Save camera parameters to JSON file and return filename"""
            
        camera_numbers = self.cam_nums
        
        printout = False # Don't print the output in fetch_camera_properties
        self.printout = printout
        
        # Write Out Camera Settings
        cam_types = self.cam_types
        cam_all_prop = self.fetch_camera_properties(cam_types, printout)
        if timestampprop:
            if timestring == '':
                timestring = '_' + time.strftime("%Y%m%d-%H%M%S")
            json_filename = cam_params_txt_file + timestring + ".txt"
        else:
            json_filename = cam_params_txt_file + ".txt"
            
        with open(json_filename, 'w') as outfile:
            json.dump(cam_all_prop, outfile, indent=4)
            print ('Camera settings saved to',os.getcwd(),'/',json_filename, sep='') 
        self.json_filename = json_filename
    
    def load_cam_params(self, json_filename):
        """Load camera parameters from JSON file"""
        if json_filename == '':
            json_filename = self.json_filename

        with open(json_filename) as json_file:
            loaded_cam_props = json.load(json_file)
        print("Properties loaded from JSON file:")
        print(loaded_cam_props)
        print()
        
        # Handle if camera numbers don't match numbers seen in json txt file
        camera_numbers = self.cam_nums
        print(camera_numbers)
        camera_properties = {}
        loaded_keys = list(loaded_cam_props)
        print("Cam prop keys into list:")
        print(loaded_keys)
        for cam_counter, camera_number in enumerate(camera_numbers):
                  
            # Create a sub-dictionary to hold current camera properties      
            camera_properties[camera_number] = {}
                  
            # Cycle through all properties from json file 
            print("Camera properties structure:\n")
            print(camera_properties)
            for prop in loaded_cam_props[loaded_keys[cam_counter]]:
                # try: 
                camera_properties[camera_number][prop] = loaded_cam_props[loaded_keys[cam_counter]][prop]
                # except:
                    # print("More cameras in json file being loaded than current cameras numbered")
        self.cam_props = camera_properties
            
        camera_numbers = self.cam_nums
        #for cam_index, camera_info in self.all_cam_props():
        for cam_index, camera_info in self.cam_props.items():
            for key in camera_info:
                subprocess.call(['v4l2-ctl -d /dev/video{} -c {}={}'.format(str(cam_index), 
                    key, str(camera_info[key]))], shell=True)
                print("Camera", cam_index, key, "has been set to ", camera_info[key])
        return self.cam_props

    def release_cameras(self): 
        """Release all cameras in object"""

        for camera in self.cameras:
            camera.stop()
        cv2.destroyAllWindows() 
        
    def camera_check(self):
        """Check video feed from all cameras"""
        
        color_on=True
        for camera in self.cameras:
            camera.start()
            print ("cameras started: ", camera)
            
        # Keep grabbing video frames until keypress
        print("Displaying live video. (The window may not display above this one, so click on it.)\nPress the 'Q' key while in the viewing window to stop live video")
        while(self.cameras[0].started):
            # Fetch data from cameras
            ret, frames = [], []
            for camera in self.cameras:
                captured, frame = camera.read()
                if not color_on:
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                ret.append(captured)
                frames.append(frame)

            # Display cameras
            if all(ret):
                for frame_num, camera_number in enumerate(self.cam_nums):
                    cam_label = "Camera " + str(camera_number)
                    cv2.imshow(cam_label,frames[frame_num])
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break  # exit if Q-key pressed
            else:
                print("\nERROR!\nCould not connect to cameras!\nEnding Recording")
                break  # exit if frames not retrieved
    
        # Release everything if job is finished   
        self.release_cameras()
        print("Video feed ending")
        cv2.destroyAllWindows()
        
    def multi_cam_capture(self, time_stamp=True, filetype='.avi',
                          show_feed=True, file_fps=30, cam_types=['']):
        """Capture video from all cameras"""
        
        # Set up filenames
        color_on=True
        timestr = ''
        filenames = []
        print("Output directory: " + self.out_dir)
        if time_stamp:
            timestr += '_' + time.strftime("%Y%m%d-%H%M%S")            
        for vid_prefix in self.vid_prefixes:
            filenames += [self.out_dir + "/" + vid_prefix + timestr + filetype]
            
        # Set up camera feeds and video files
        videos = []
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        camera_numbers = self.cam_nums
        
        for index, filename in enumerate(filenames):
            videos += [cv2.VideoWriter(filename, fourcc, file_fps, (self.width,self.height))]
            self.cameras[index].start()
            current_cam_num = str(camera_numbers[index])
            # Set the fps
            termcmd = "v4l2-ctl -d /dev/video{} -p ".format(current_cam_num) + str(file_fps) 
            ps = subprocess.Popen(termcmd,
                                  shell=True,
                                  stdout=subprocess.PIPE,
                                  stderr=subprocess.STDOUT,
                                  universal_newlines=True)
            output = ps.communicate()[0]
            print ("camera_number" + str(camera_numbers[index]) + " had it's " + str(output).lower())
        
        #Set up current timestamp data array
        timestamps_now = np.zeros((1,len(self.cam_nums)))
        capturedprev, frameprev = self.cameras[0].read()
        
        # Hash current frame 
        hashprev = hashlib.md5(frameprev)
        
        # Show current frame
        cv2.imshow('Video Feed',frameprev)
    
        # Keep grabbing video frames until keypress   
        while(self.cameras[0].started):
            # Fetch data from cameras
            ret, frames = [], []
            for camera_number, camera in enumerate(self.cameras):
                captured, frame = camera.read()
                if not color_on:
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                ret.append(captured)
                frames.append(frame)
                timestamps_now[0,camera_number] = time.time()

            # Write to file if data received from all cameras
            if all(ret):
                # Check hash of current frame aginst the last check
                framecurr = frame[0]
                hashcurr = hashlib.md5(framecurr)
                if hashcurr.hexdigest() != hashprev.hexdigest():
                    for vid_num, video in enumerate(videos):
                        video.write(frames[vid_num])  # write frame
                    try:
                        timestamps_all = np.append(timestamps_all,
                        timestamps_now, axis=0)
                    except:
                        print("Initializing timestamps")
                        timestamps_all = np.copy(timestamps_now)
                    if show_feed:
                        cv2.imshow('Video Feed',frames[0])
                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break  # exit if Q-key pressed
                    hashprev = hashcurr
            else:
                print("\nERROR!\nCould not connect to cameras!\nEnding Recording")
                break  # exit frames not retrieved

        # Release everything if job is finished  
        self.release_cameras()
        print("Video feed ending")
        # cv2.destroyAllWindows()    
        
        #Clean up timestamp data
        num_frames = timestamps_all.shape[0]
        print("Captured " + str(num_frames) + " frames")
        time_init = np.copy(timestamps_all[0,0])
        for index in range(num_frames):
            timestamps_all[index,:] = np.copy(timestamps_all[index,:] - time_init)    
        timestamps_file_name = self.out_dir + "/timestamps" +  timestr + ".csv"
        np.savetxt(timestamps_file_name, timestamps_all, delimiter=",") 
        
        self.timestamps_file_name = timestamps_file_name
        
        # Save Camera Properties
        camparams_file_name = self.out_dir + "/camera_parameters" +  timestr
        cam_props_filename = self.save_cam_params(cam_params_txt_file=camparams_file_name, printout = False)
        return filenames, timestamps_file_name, cam_props_filename


def visualize_framerate(timestamps_file_name=''):
    """Visualize the performance of the video capture over time"""

    # Attempt to find old timestamps file
    if timestamps_file_name == '':
        timestamps_file_name = self.timestamps_file_name

    # Load file and allocate variables
    timestamps = np.loadtxt(timestamps_file_name,dtype=float,delimiter=',')
    n_frames = timestamps.shape[0]
    n_differences = n_frames - 1
    try:
        n_cams = timestamps.shape[1]
    except:
        n_cams = 1
    time_diffs = np.empty([n_differences, n_cams])
    cam_diffs = np.empty([n_differences, 1])

    # Sometimes cameras can take a while to normalize. Setting this will allow you to analize the data after
    # the cameras have stabalized
    initial_frames_to_skip = 0

    for frame in range(n_differences):
        for cam in range(n_cams):
            if n_cams == 1:
                time_diffs[frame, cam] = timestamps[frame+1] - timestamps[frame]
            else:
                time_diffs[frame, cam] = timestamps[frame+1,cam] - timestamps[frame,cam]

    for cam in range(n_cams):
        frames_per_second = np.empty(time_diffs.shape)
        frames_per_second = np.reciprocal(time_diffs)
        print("Camera " + str(cam+1) +":")
        print("Average fps: " + str(np.mean(frames_per_second[initial_frames_to_skip:,cam])))
        print("Minimum fps: " + str(np.min(frames_per_second[initial_frames_to_skip:,cam])))
        print("Maximum fps: " + str(np.max(frames_per_second[initial_frames_to_skip:,cam])))
        print("Standard deviation of fps: " + str(np.std(frames_per_second[:,cam])))
        print("")
        plt.plot(frames_per_second[initial_frames_to_skip:,cam])
        plt.show()
        #plt.hist(frames_per_second[initial_frames_to_skip:,cam])
        #plt.show()

    #Time Differences Between Cameras
    t_last = np.empty([n_frames, 1])
    t_first = np.empty([n_frames, 1])
    cam_diffs = np.empty([n_frames, 1])
    if n_cams == 1:
        t_last[:,0] = np.copy(timestamps[:])
        t_first[:,0] = np.copy(timestamps[:])
    else:
        t_last[:,0] = np.copy(timestamps[:,n_cams-1])
        t_first[:,0] = np.copy(timestamps[:,0])
    cam_diffs = t_last - t_first

    if n_cams > 1:
        print("Inter Camera Differences in milliseconds:")
        print("Average offset: " + str(1000*np.mean(cam_diffs[initial_frames_to_skip:,0])))
        print("Minimum offset: " + str(1000*np.min(cam_diffs[initial_frames_to_skip:,0])))
        print("Maximum affset: " + str(1000*np.max(cam_diffs[initial_frames_to_skip:,0])))
        print("Standard deviation of offset: " + str(1000*np.std(cam_diffs[:,0])))
        print("")

        plt.plot(1000*cam_diffs)
        plt.show()
    else:
        print("Not enought cameras to compute inter camera frame difference")
    
# Please acknowledge our Core in your publications. An appropriate wording would be: “Engineering support was provided by the Optogenetics and Neural Engineering Core at the University of Colorado Anschutz Medical Campus, funded in part by the National Institute of Neurological Disorders and Stroke of the National Institutes of Health under award number P30NS048154."

