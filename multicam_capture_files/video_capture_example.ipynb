{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "# Capture Multiple Timestamped Videos Using USB Webcams\r\n",
    "\r\n",
    "This Notebook runs as an example of a laptop running Ubuntu with a webcam and two [Playstation Eye cameras](https://optogeneticsandneuralengineeringcore.gitlab.io/ONECoreSite/projects/PS3EyeCamera/) attached. The user will record only from the PS3Eye cameras. Modifications to this setup (like recording from three PS3Eye cameras) can be made by the user. The user need not understand all the code, and we are hopeful our documentation shows you how to quickly change the parameters to best suit your specific needs.\r\n",
    "\r\n",
    "We have tested this code explicitly with the PS3Eye camera and the Logitech C270 camera. Other basic webcams should work with this script, but take care to note that settings (such as resolution, frame rate, and exposure) are unique for each USB camera. \r\n",
    "\r\n",
    "To optimize performance, close all other programs on your computer and unnecessary tabs in your browser before running.\r\n",
    "\r\n",
    "------------------\r\n",
    "\r\n",
    "### Workflow:\r\n",
    "\r\n",
    "    1. Determine cameras detected by the system\r\n",
    "    2. Initialize the cameras\r\n",
    "    3. Load approproate camera settings\r\n",
    "    4. View the camera feed(s) for visual check of angles and settings\r\n",
    "    5. Capture videos\r\n",
    "    6. Visualize framerate to get an idea of performance\r\n",
    "    "
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Troubleshooting Tip\r\n",
    "\r\n",
    "The following is one common error that can occur:\r\n",
    "`An error of AttributeError: 'NoneType' object has no attribute 'copy'`\r\n",
    "\r\n",
    "The error is indicative that communication to the webcam has been interrupted. The most reliable solution to this is to unplug the webcams, plug them back in again, and restart the Jupyter Notebook kernel. "
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Import Camera Capture Code"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "# Import Library with Camera Classes\r\n",
    "import os\r\n",
    "import sys\r\n",
    "print(os.sys.path)\r\n",
    "sys.path.append('/usr/local/lib/python3.8/dist-packages')\r\n",
    "\r\n",
    "import video_capture_classes as vc"
   ],
   "outputs": [],
   "metadata": {
    "scrolled": true
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "# View Cameras Detected by the System\r\n",
    "\r\n",
    "The `list_usb_devices()` function has the system list all detected USB devices connected to the computer (even webcams are detected as USB devices). It should list them as something like \"/dev/video0\", with the number after 'video' being the camera number. If your computer has a built-in webcam, this will typically be camera number zero. Cameras connected to Ubuntu are listed as integers, starting at zero.\r\n",
    "\r\n",
    "It can be helpful to run this line before connecting the cameras to the system and then to rerun it after plugging in each camera to know which number is associated with which camera, and to make sure that cameras' numbers have not changed since the last use of this code (this can frequently happen if you unplug and reconnect a camera while the computer is still on)."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "vc.list_usb_devices()"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Initialize the Cameras\r\n",
    "\r\n",
    "Having used `list_usb_devices()`, we know the camera number(s) of the camera(s) we want to record from. We then pass a list of these camera numbers in as the \"cam_nums\" argument when we initialize our cameras. Here are a few different examples of how to specify which cameras to record from:\r\n",
    "\r\n",
    "``` \r\n",
    "cam_nums = [0, 1]     # Use first two cameras\r\n",
    "cam_nums = [0, 1, 2]  # Use first three cameras\r\n",
    "cam_nums = [1, 2]     # Use second and third camera\r\n",
    "```\r\n",
    "\r\n",
    "The `width` and `height` parameters will dictate the resolution of the video. Save yourself some trouble and look up what resolutions your USB webcam supports before inputting your best guess. These default to a width of 640 and a height of 480. Currently all cameras must be recorded with the same resolution.\r\n",
    "\r\n",
    "The `out_dir` parameter is the directory that the output files will be written to. It defaults to the current directory.\r\n",
    "``` \r\n",
    "out_dir=None                     # videos will be saved to the current directory\r\n",
    "out_dir='/home/username/Videos'   # /home/username/Videos will be used, assuming you have a Videos directory\r\n",
    "```\r\n",
    "The `vid_prefixes` will add a prefix to the filenames we are saving our .avi's as. If no prefix is given, filenames will default to 'video_cam' + the camera number (from cam_nums above).  Prefix names are assigned to the cam_nums as they appear in `list_usb_devices()`. The second example below would work *the exact same* if the first two cameras are chosen, or if the second and third cameras are chosen (in cam_nums). *If you specify prefixes, the number of cameras must match the number of prefixes!*\r\n",
    "``` \r\n",
    "vid_prefixes=[]  # for all cameras selected above, each filename will default to \"video_cam\" + camera number\r\n",
    "vid_prefixes=['Right', 'Left']  # two .avi's will be saved with 'Right' and 'Left' as the prefixes\r\n",
    "``` "
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "Cameras = vc.MultiCam(cam_nums=[1,2], width=640, height=480, vid_prefixes=['eye_cam1', 'eye_cam2'], out_dir=None)"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Camera Parameter Discussion\r\n",
    "\r\n",
    "\r\n",
    "### See your options\r\n",
    "\r\n",
    "Different USB cameras have different settings that can be changed. Check your available camera options wth the code below or by opening up a terminal and check the output of `v4l2-ctl --device /dev/video[num] --all`, where [num] is the number of the camera.\r\n",
    "\r\n",
    "### Example: Setting Options for the PS3Eye\r\n",
    "\r\n",
    "                brightness(int)  :  min=0 max=255 step=1 default=0 value=0 flags=slider  \r\n",
    "                 contrast (int)  :  min=0 max=255 step=1 default=32 value=32 flags=slider  \r\n",
    "                saturation(int)  :  min=0 max=255 step=1 default=64 value=64 flags=slider  \r\n",
    "                      hue (int)  :  min=-90 max=90 step=1 default=0 value=0 flags=slider  \r\n",
    "                 exposure (int)  :  min=0 max=255 step=1 default=120 value=120 flags=inactive, volatile  \r\n",
    "          gain_automatic (bool)  :  default=1 value=1 flags=update  \r\n",
    "                     gain (int)  :  min=0 max=63 step=1 default=20 value=20 flags=inactive, volatile  \r\n",
    "         horizontal_flip (bool)  :  default=0 value=0  \r\n",
    "           vertical_flip (bool)  :  default=0 value=0  \r\n",
    "    power_line_frequency (menu)  :  min=0 max=1 default=0 value=0  \r\n",
    "                 sharpness(int)  :  min=0 max=63 step=1 default=0 value=0 flags=slider\r\n",
    "\r\n",
    "### Automatic Settings\r\n",
    "\r\n",
    "By default, many USB cameras will automatically adjust several settings such as white balance and exposure. While sometimes handy, we generally recommend turning off these automatic settings and manually setting parameters that work best for your needs. We have had instances where the image background is significantly darker or brighter than the feature of interest, and the automatically set exposure results in over- or under-exposure. \r\n",
    "\r\n",
    "### Resolution and Working Modes\r\n",
    "\r\n",
    "Camera data throughput can be limited by the data transfer rate of the connection (ex: PS3Eye uses a USB 2.0 connection). The PS3Eye camera is reported to work for the following resolution and frame rates:\r\n",
    "\r\n",
    "00: 640x480@15  \r\n",
    "01: 640x480@30  \r\n",
    "02: 640x480@40  \r\n",
    "03: 640x480@50  \r\n",
    "04: 640x480@60  \r\n",
    "10: 320x240@30  \r\n",
    "11: 320x240@40  \r\n",
    "12: 320x240@50  \r\n",
    "13: 320x240@60   \r\n",
    "14: 320x240@75  \r\n",
    "15: 320x240@100  \r\n",
    "16: 320x240@125  "
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Fetching, Viewing, and Saving Camera Parameters\r\n",
    "\r\n",
    "The `fetch_camera_properties()` method will get all the current camera property values as they are. \r\n",
    "\r\n",
    "As mentioned before, we have tested this code with two types of cameras: The PS3Eye and the Logitech C270. Other USB cameras may also work, but altering the camera properties may require looking at the code (see the `fetch_camera_properties` method within the `video_capture_classes.py` file for more information).\r\n",
    "\r\n",
    "``` \r\n",
    "cam_types=['ps3eye']       # Will use PS3Eye camera settings\r\n",
    "cam_types=['logitech']     # Will use Logitech C270 camera settings\r\n",
    "cam_types=[]               # Any other webcam can be used, but updating settings isn't as straightforward\r\n",
    "```\r\n",
    "\r\n",
    "`Cameras.camera_check()` will display (not record) the cameras in *new windows*. Press Q to exit.\r\n",
    "\r\n",
    "`Cameras.save_cam_params()` will save the fetched camera properties as a JSON .txt file. \r\n",
    "\r\n",
    "``` \r\n",
    "cam_params_txt_file='camera_params'   # Prefix for save your camera setting\r\n",
    "timestampprop=True                    # Adds a timestamp to the name of your camera settings file\r\n",
    "```\r\n",
    "\r\n",
    "\r\n",
    "***Note! The live video windows may appear in a window 'behind' the notebook. Click on the live feed window to make it the currrent window and then press the 'Q' key while in the viewing window to stop live video. During viewing and recording, using the \"X\" window button to close the window will not stop the feed, only cause the window to close and open again.*** "
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "Cameras.fetch_camera_properties(cam_types=['ps3eye'])\r\n",
    "\r\n",
    "Cameras.camera_check()\r\n",
    "\r\n",
    "Cameras.save_cam_params(cam_params_txt_file='default_params', timestampprop=True)"
   ],
   "outputs": [],
   "metadata": {
    "scrolled": true
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Editing Parameters\r\n",
    "\r\n",
    "To edit the properties of the cameras, open the parameters JSON text file above in your favorite text editor, make your desired changes, and save the file.\r\n",
    "\r\n",
    "# Loading and Checking Parameters\r\n",
    "\r\n",
    "Use the `load_cam_params()` method to load in a camera settings file.\r\n",
    "\r\n",
    "```\r\n",
    "Cameras.load_cam_params(json_filename='')  # attempts to load parameters saved from `Cameras.save_cam_params()`\r\n",
    "Cameras.load_cam_params(json_filename ='default_params_20200402-131138.txt')  # loads camera parameters saved with this filename\r\n",
    "```\r\n",
    "\r\n",
    "The `load_cam_params()` method loads the first set of camera properties in the loaded file's saved dictionary to the first camera currently being controlled, the second set to the second camera, and so on.  For example, if the default camera numbers found in the JSON file are [0, 1] and the current camera numbers are [2, 3], then \"Camera 2\" will load the first cam profile (0) found in the json file, while \"Camera 3\" will load the second profile (1). For this reason, it is recommended that users maintain the same numeric order of cameras, and load parameter files that have the same number of camera property dictionaries as the number of cameras being used.\r\n",
    "\r\n",
    "With everything loaded, we can then take a look at a live stream of the camera using the `camera_check()` method. If something is off, we can change the settings file we were loading, load it again, and reinspect the live feed to see if that got us what we wanted. This is useful for tweaking parameters such as exposure, gain, brightness, contrast, etc..."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "Cameras.load_cam_params(json_filename ='default_ps3eye_params.txt')\r\n",
    "\r\n",
    "Cameras.camera_check()"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "***Note! Sometimes the camera live feed ends up having a slightly different appearance from the actual video files. Usually this difference is in the form of the `camera_check()` live feed appearing brighter than the actual recorded video (which is then too dark). It is therefore recommended that you record a brief video to see if the brightness camera parameter needs to be adjusted for the actual recording.*** "
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Capture Videos\r\n",
    "\r\n",
    "Use the `multi_cam_capture()` method to record video(s). Press the \"Q\" key to stop recording the video(s) *(Again, using the \"X\" button to close the video feed window will not stop the recording, just reload the window)*.\r\n",
    "\r\n",
    "`show_feed` : If set to 'True', feed(s) from the videos will be displayed. May cause speed issues with slower systems.\r\n",
    "\r\n",
    "`time_stamp` : If set to 'True', the code will generate a .cvs file that records when each image in a video was recorded. If set to 'False', no timestamp file is generated.\r\n",
    "\r\n",
    "`file_fps` : Sets the *playback* framerate of the video(s). This is the metadata that will determine how many frames per second video players will play the file at. For example, if your camera is set to capture at 60fps but the 'file_fps' is set to 120fps, the resulting video would play in double speed in most video players."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "video_files, timestamp_file, cam_params_file = Cameras.multi_cam_capture(show_feed=True, time_stamp=True, file_fps=30) "
   ],
   "outputs": [],
   "metadata": {
    "scrolled": true
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Visualize Framerate\r\n",
    "\r\n",
    "Depending on several factors, the actual video capture performance may not be as good as hoped. Use the `visualize_framerate()` method to get an idea how things performed.\r\n",
    "\r\n",
    "The timestamps file generated by the multi_cap_capture method is a simple .csv file with one column for each camera used. Each row represents a set of frames captured 'simultaneously' across all the cameras. There is a timestamp in seconds for each frame captured from each camera. Comparing timestamps within a single column shows how smooth the framerate was for each camera, and comparing timestamps across rows shows just how 'in sync' the cameras actually were. \r\n",
    "\r\n",
    "The first set of figures created by this method illustrates each individual column of data. Ideally, this will be a simple flat line resting at the expected framerate.\r\n",
    "\r\n",
    "The last figure generated illustrates how 'in sync' the cameras are. The numbers shown are the difference in time from the first 'synchronized' frame captured compared to the timestamp of the last 'synchronized' frame. Ideally this will be very close to zero.\r\n",
    "\r\n",
    "**Note that you need to input the location of the timestamps file (ex: ~/Videos/timestamp-filename).**"
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "Cameras.visualize_framerate(timestamp_file)"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "An example timestamps file is included with this Notebook. It can be passed into the `visualize_framerate()` method with the following line (unless it has been moved, in which case you will need to include its location):\r\n",
    "`Cameras.visualize_framerate('timestamps_example.csv')`"
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "### Please acknowledge our core in your publications. An appropriate wording would be:\r\n",
    "\r\n",
    "“The Optogenetics and Neural Engineering (ONE) Core at the University of Colorado School of Medicine provided engineering support for this research. The ONE Core is part of the NeuroTechnology Center, funded in part by the School of Medicine and by the National Institute of Neurological Disorders and Stroke of the National Institutes of Health under award number P30NS048154.”"
   ],
   "metadata": {}
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}